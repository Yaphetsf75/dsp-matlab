        %%%%%bakhshe 1
    %%%%alef
Fs=8000;    %or 6k !
f=1000;
t=1;
phi=0;
m= 0:(1/Fs):t;
y=sin(2*pi*f*m+phi);
sound (y,Fs)
n=0:(1/Fs):5/f;
z=sin(2*pi*f*n+phi);
figure
stem(n,z)
title('Fs=8k    f=1k');
xlabel('Time');
ylabel('y=sin(2*pi*f*m+phi)');
%%
    %%%%b
Fs=2000;
f=1000;
t=1;
phi=rand(1);
m= 0:(1/Fs):t;
y=sin(2*pi*f*m+phi);
sound (y,Fs)
n=0:(1/Fs):5/f;
z=sin(2*pi*f*n+phi);
figure
stem(n,z)
title('Fs=2k    f=1k');
xlabel('Time');
ylabel('y=sin(2*pi*f*m+phi)');

%%
    %%%%p
Fs=1500;    %or 6k !
f=1000;
t=1;
phi=rand(1);
m= 0:(1/Fs):t;
y=sin(2*pi*f*m+phi);
sound (y,Fs)
n=0:(1/Fs):5/f;
z=sin(2*pi*f*n+phi);
figure
stem(n,z)
title('Fs=1.5k    f=1k');
xlabel('Time');
ylabel('y=sin(2*pi*f*m+phi)');
%%
    %%%%d
Fs=2500;
f=1000;
t=10;
a=10^-5;
phi=rand(1);
m= 0:(1/Fs):t;
y=sin(2*pi*f*m+phi);
sound (y,Fs)
r=2*a*rand(1,length(m))-a+1;
m=m.*r;
y=sin(2*pi*f*m+phi);
sound (y,Fs)
n=0:(1/Fs):5/f;
z=sin(2*pi*f*n+phi);
figure
stem(n,z)
title('Fs=2.5k    f=1k');
xlabel('Time');
ylabel('y=sin(2*pi*f*m+phi)');
%%
    %%%%h1
Fs=8000;
f=1000;
t=1;
b=14;
up=1;
down=-1;
phi=rand(1);
m= 0:(1/Fs):t;
y=sin(2*pi*f*m+phi);
u=quantiz(b,y,up,down);
sound (u,Fs)
n=0:(1/Fs):5/f;
z=sin(2*pi*f*n+phi);
o=quantiz(b,z,up,down);
figure
subplot(3,2,1)
stem(n,z)
title('Fs=8k    f=1k');
xlabel('Time');
ylabel('y=sin(2*pi*f*m+phi)');
subplot(3,2,2)
stem(n,o)
title('Fs=8k    f=1k');
xlabel('Time');
ylabel('y=sin(2*pi*f*m+phi)');
l=z-o;
subplot(3,1,2)
stem(n,l)
title('Fs=8k    f=1k');
xlabel('Time');
ylabel('y=sin(2*pi*f*m+phi)');
w=-pi:pi/100:pi;
h=zeros(1,length(w));
g=zeros(1,length(w));
for i=1:length(w)
    h(i)=abs(DTFT(y,w(i)));
    g(i)=abs(DTFT(u,w(i)));
end
subplot(3,2,5);
plot(w,h);
title('dtft of signal');
xlabel('omega');
ylabel('dtft');
subplot(3,2,6);
plot(w,g);
title('dtft of quantized signal');
xlabel('omega');
ylabel('dtft');
%%
    %%%%hh
    bits=25;
p=zeros(1,bits);
for b = 1:bits
    Fs=8000;
    f=1000;
    t=1;
    up=1;
    down=-1;
    phi=rand(1);
    m= 0:(1/Fs):t;
    y=sin(2*pi*f*m+phi);
    u=quantiz(b,y,up,down);
    %sound (u,Fs)
    sp=0;np=0;
    for v=1:length(m)
        sp=sp+y(v)^2;
        np=np+(y(v)-u(v))^2;
    end
    p(b)=10*log10(sp/np);
end
v=1:bits;
figure
plot(v,p);
title('SNR');
xlabel('Number of bits');
ylabel('SNR');
polyfit(v,p,1)
%%
    %%%%%hhh
    p=zeros(1,150);
j=zeros(1,150);
for i=1:150
    Fs=3000+200*i;
    b=16;
    f=1000;
    j(i)=Fs/(2*f);
    t=1;
    up=1;
    down=-1;
    phi=rand(1);
    m= 0:(1/Fs):t;
    y=sin(2*pi*f*m+phi);
    u=quantiz(b,y,up,down);
    %sound (u,Fs)
    sp=0;np=0;
    for v=1:length(m)
        sp=sp+y(v)^2;
        np=np+(y(v)-u(v))^2;
    end
    p(i)=10*log10(sp/np);
end
figure
plot(j,p)
%%
    %%%%end