function [ y ] = downsample( x , n )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
p=size(x);
y=zeros(ceil(p(1)/n),p(2));
r=1;
for i=1:p(1)
    if rem(i,n) == 1
        for j=1:p(2)
            y(r,j)=x(i,j);
        end
        r=r+1;
    end
end

end

