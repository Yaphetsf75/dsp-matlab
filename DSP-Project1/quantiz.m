function [ y ] = quantiz( n,x,up,down )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
p=length(x);
y=zeros(1,p);
for m=1:1:p
    if x(m)>= up
        y(m)=up;
    end
    if x(m)<=down
        y(m)=down;
    end
    if x(m) < up && x(m) > down
        if floor(2*x(m)*(2^n)/(up-down)) > 2*floor(x(m)*(2^n)/(up-down))
            j=1;
        else
            j=0;
        end
        y(m)=(floor(x(m)*(2^n)/(up-down))+j)*(up-down)/(2^n);
    end
end
end

