function [ y ] = FFT3_prob2( x )
x_0=downsample(x,3);
z=x(2:length(x));
x_1=downsample(z,3);
z=x(3:length(x));
x_2=downsample(z,3);
if(length(x) ~= 3)
    g0=FFT3_prob2(x_0);
    g1=FFT3_prob2(x_1);
    g2=FFT3_prob2(x_2);
    k=0:length(x)-1;
    w1=exp(-1i*2*pi*k/length(x));
    w2=exp(-1i*4*pi*k/length(x));
    y=repmat(g0,[1,3])+w1.*repmat(g1,[1,3])+w2.*repmat(g2,[1,3]);
else
    y(1)=x(1)+x(2)+x(3);
    y(2)=x(1)+x(2)*exp(-1i*2*pi/3)+x(3)*exp(-1i*4*pi/3);
    y(3)=x(1)+x(2)*exp(-1i*4*pi/3)+x(3)*exp(-1i*8*pi/3);
end
end

