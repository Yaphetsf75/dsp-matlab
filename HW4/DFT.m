function [ y , t] = DFT( x )
N=length(x);
n = 0:1:N-1;
k = 0:1:N-1;
WN = exp(-1i*2*pi/N);
nk = transpose(n)*k; 
WNnk = WN .^ nk; 
tic
y = x * WNnk;
t=toc;
end

