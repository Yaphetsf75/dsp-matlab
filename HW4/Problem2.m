%%
i=10:10000;
j=10:10000;
r=10:10000;
t=zeros(1,length(i));
tt=zeros(1,length(i));
for i=10:10000
    if ( sum(factor(i)==2)+sum(factor(i)==3)==length(factor(i)) )
        x=floor(10*rand(1,i)-10);
        [y,t(i)]=FFT6_prob2(x);
        [y,tt(i)]=DFT(x);
    end
end
i=10:10000;
%%
r=r(t~=0);
i=i(t~=0);
j=j(t~=0);
plot(log10(i),log10(t),log10(j),log10(tt),log10(r),log10(r.*log10(r)))
xlabel('log(N)');ylabel('log(time)');
legend('FFT' ,'DFT' ,'log(Nlog(N))');