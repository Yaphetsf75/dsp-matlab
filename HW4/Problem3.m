%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PART A%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for a=0.001:0.03:0.1
    m=200;
    n=1:m;
    y=cos(2*pi*a*n.^2);
    N_FFT=100;
    Y=fftshift(fft(y(1:100),100));
    fVals=pi*(-N_FFT/2:N_FFT/2-1)/N_FFT;
    figure
    stem(fVals,abs(Y))
end
%%
a=0.001;
m=200;
n=1:m;
w=20;
y=cos(2*pi*a*n.^2);
l=m-w+1; %number of possible windows
H=zeros(l,w);
for i=1:l
    H(i,:)= fftshift(fft(y(i:i+w-1),w));
end
a=0.003;
y=cos(2*pi*a*n.^2);
J=zeros(l,w);
for p=1:l
    J(p,:)=fftshift(fft(y(p:p+w-1),w));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PART B%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
loops=l;
F(loops) = struct('cdata',[],'colormap',[]);
N_FFT=w;
fVals=pi*(-N_FFT/2:N_FFT/2-1)/N_FFT;
for c=1:l
    stem(fVals,abs(H(c,:)))
    hold on
    stem(fVals,abs(J(c,:)))
    F(c)=getframe;
    close
end
%%
movie(F,1,2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PART C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
loops=w;
G(loops) = struct('cdata',[],'colormap',[]);
N_FFT=l;
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
for c=1:w
    stem(fVals,abs(H(:,c)))
    hold on
    stem(fVals,abs(J(:,c)))
    G(c)=getframe;
    close
end
%%
movie(G,1)
%%
%%
loops=w/2;
U(loops) = struct('cdata',[],'colormap',[]);
N_FFT=l;
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
for c=1:w/2
    stem(fVals,abs(H(:,2*c)))
    hold on
    stem(fVals,abs(J(:,2*c)))
    ylim([0,12])
    U(c)=getframe(gcf);
    close
end
%%
movie(U,1)
%%
movie2avi(U, 'myPeakk.avi', 'compression', 'None','fps',5);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PART D%%%%%%%%%%%%%%%%%%%%%%%%
%%
[ note, Fs ] = audioread( 'note.wav' );
note = transpose(note( :, 1 ));
w=1000;
b=ceil(length(note)/w);
NO(b)=struct('cdata',[],'colormap',[]);
for i=1:b
    N_FFT=w;
    Y=fftshift(fft(note(i:i+w-1),w));
    fVals=Fs*(-N_FFT/2:N_FFT/2-1)/N_FFT;
    plot(fVals,abs(Y));
    NO(i)=getframe(gcf);
end
movie(NO,1);