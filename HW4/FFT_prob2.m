function [ y ] = FFT_prob2( x )
y=zeros(1,length(x));
x_0=downsample(x,2);
z=x(2:length(x));
x_1=downsample(z,2);
if(length(x) ~= 2)
    g=FFT_prob2(x_0);
    h=FFT_prob2(x_1);
    k=0:length(x)-1;
    w=exp(-1i*2*pi*k/length(x));
    y=repmat(g,[1,2])+w.*repmat(h,[1,2]);
else
    y(1)=sum(x);
    y(2)=x(1)-x(2);
end
end

