y1=imread('P3a.jpg');   y2=imread('P3b.jpg');
%%
y1=rgb2gray(y1);    y2=rgb2gray(y2);
%%
mf = [0 0 0;0 3 0;0 0 0];
y3=imfilter(y1,mf);
y4=imfilter(y2,mf);
figure
subplot(121);imshow(y3);subplot(122);imshow(y4);
%%
y3=medfilt2(y3);
y4=medfilt2(y4);
figure
subplot(121);imshow(y3);subplot(122);imshow(y4);
%% watershed segmentation algorithm
y3g = im2double(y3g);
y3x = imfilter(double(y3g), (fspecial('sobel')), 'replicate');
y3y = imfilter(double(y3g), (fspecial('sobel'))', 'replicate');
gy3 = sqrt(y3x.^2 + y3y.^2);
y4g = im2double(y4g);
y4x = imfilter(double(y4g), (fspecial('sobel')), 'replicate');
y4y = imfilter(double(y4g), (fspecial('sobel'))', 'replicate');
gy4 = sqrt(y4x.^2 + y4y.^2);
figure
subplot(121);imshow(gy3,[])
subplot(122);imshow(gy4,[])
figure
subplot(121)
imshow((label2rgb(watershed(gy3))))
subplot(122)
imshow((label2rgb(watershed(gy4))))
%% Threshold
figure
subplot(121);imshow(imopen(y1, strel('disk', 10))), title('Opening (I)')
subplot(122);imshow(imopen(y2, strel('disk', 10))), title('Opening (I)')
y3 = imreconstruct(imerode(y1, strel('disk', 10)), y1);
y4 = imreconstruct(imerode(y2, strel('disk', 10)), y2);
figure
subplot(121);imshow(y3)
subplot(121);imshow(y4)
y33 = imdilate(y3, strel('disk', 10));
y44 = imdilate(y4, strel('disk', 10));
y333 = imreconstruct(imcomplement(y33), imcomplement(y3));
y444 = imreconstruct(imcomplement(y44), imcomplement(y4));
y333 = imcomplement(y333);
y444 = imcomplement(y444);
figure
subplot(121);imshow(y333)
subplot(122);imshow(y444)
figure
subplot(121);imshow(imregionalmax(y333))
subplot(122);imshow(imregionalmax(y444))
y3 = y1;
y4 = y2;
y3(imregionalmax(y333)) = 255;
y4(imregionalmax(y444)) = 255;
figure
subplot(121);imshow(y3)
subplot(122);imshow(y4)
%% final picture of tumor if there's any
if sum(sum(im2bw(y333, 0.5)))>100
    disp('Yes');
else
    disp('No');
end
if sum(sum(im2bw(Iobrcbr, 0.5)))>100
    disp('Yes');
else
    disp('No');
end
