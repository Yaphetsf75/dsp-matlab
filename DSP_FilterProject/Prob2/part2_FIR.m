close all;
[y,fs]=audioread('Owl.wav');
n=1:length(y);
yf=fftshift(fft(y));
N_FFT=length(y);
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
figure
plot(fs*fVals,abs(yf));
%%
J1=filter(FIR_l800,y);
audiowrite('FJ1.wav',J1,fs);
J1f=fftshift(fft(J1));
N_FFT=length(J1);
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
figure
plot(fs*fVals,abs(J1f));
hold on
%%
J2=filter(FIR_b1100,y);
audiowrite('FJ2.wav',J2,fs);
J2f=fftshift(fft(J2));
N_FFT=length(J2);
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
plot(fs*fVals,abs(J2f));
hold on
%%
J3=filter(FIR_b1300,y);
audiowrite('FJ3.wav',J3,fs);
J3f=fftshift(fft(J3));
N_FFT=length(J3);
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
plot(fs*fVals,abs(J3f));
hold on
%%
J4=filter(FIR_b1600,y);
audiowrite('FJ4.wav',J4,fs);
J4f=fftshift(fft(J4));
N_FFT=length(J4);
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
plot(fs*fVals,abs(J4f));
%%
fvtool(FIR_l800)
fvtool(FIR_b1100)
fvtool(FIR_b1300)
fvtool(FIR_b1600)
