close all;
load('Radarr');
Rf=fftshift(fft(Radarr));
N_FFT=length(Radarr);
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
Radar_filt=filter(Chebyshev,Radarr);
Rfilt_f=fftshift(fft(Radar_filt));
N_FFT=length(Radar_filt);
fVals=(-N_FFT/2:N_FFT/2-1)/N_FFT;
figure;
plot(Radar_filt);
hold on
plot(Radarr);
legend('Detected' ,'Recieved');
figure;
plot(fVals,abs(Rfilt_f));
hold on
plot(fVals,abs(Rf));
legend('Detected' ,'Recieved');
fvtool(Chebyshev)   % ALL OF THE PARAMETERS OF FILTER IS IN THIS FILE !
                    % YOU CAN SEE MAGNITUDE/PHASE RESPONSE AND Z PLANE HERE
