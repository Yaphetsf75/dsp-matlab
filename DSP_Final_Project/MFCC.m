function [Cepstrum] = MFCC(filename)
[Input_Audio,Fs] = audioread(filename);
N = 256;                                                                %blocking
M = 100;
Num_Frames = floor((length(Input_Audio)-M)/(N-M));
Frames = zeros(N,Num_Frames);
for i=1:Num_Frames
    Frames(:,i) = Input_Audio((i-1)*(N-M)+1:(i-1)*(N-M)+N);
end
W = hamming(N);                                                         %windowing
Windowed_Frames = (Frames.*repmat(W,1,Num_Frames));
Pow_Spec = abs(fft(Windowed_Frames)).^2;                                %FFT
Mel_Spec = melfb(Pow_Spec,Fs,Num_Frames);                                          %Mel_Wrapping
Cepstrum = dct(log(Mel_Spec'))';                                        %cepstrum
end