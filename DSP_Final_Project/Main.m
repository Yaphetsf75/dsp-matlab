clear all
%%
for i=1:8
    file{i}=['TRAIN/s',num2str(i),'.wav'];
    C{i}=vqlbg(MFCC(file{i}),20);
end
for i=1:8
    testfile{i}=['TEST/s',num2str(i),'.wav'];
    Cepstrum{i}=MFCC(testfile{i});
    for n=1:8
        
        for m=1:size(Cepstrum{i},2)
            
            for p = 1: size(C{n},2)
                Dist(p)=sum((C{n}(:,p)-Cepstrum{i}(:,m)).^2);
            end
            Dist_min(m)= min(Dist);
        end
        Distor(i,n)=meansqr(Dist_min);
        
    end
%     [~,index(i)]=min(Distor);
%     Final_ans(i)=index;
%     clear Distor
end
for i=1:8
    [~,B]=min(Distor);
    [~,indd]=min(min(Distor));
    Final_ans(B(indd))=indd;
    Distor(:,indd)=1e7;
    Distor(B(indd),:)=1e7;
end
    
% plot(S{1}(5,:),S{1}(6,:),'rs','LineWidth',0.001,'MarkerSize',3);
% hold on
% plot(S{2}(5,:),S{2}(6,:),'bs','LineWidth',0.001,'MarkerSize',3);
% hold on
% plot(CB{1}(5,:),CB{2}(6,:),'ys','LineWidth',0.001,'MarkerSize',3);
% hold on
% plot(CB{2}(5,:),CB{1}(6,:),'ks','LineWidth',0.001,'MarkerSize',3);
% legend('S{1}','S{2}','CB{1}','CB{2}','Location','northwest')
