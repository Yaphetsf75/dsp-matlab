function [Mel_Spec] = MELFB(Pow_Spec,Fs,Num_Frames)                             %Mel_Frequency_Wrapping
Flow = 80;                                                              
Mel_Flow = 1125*log(1+Flow/700);
K = 20;                                                              %Num of Filters in Fil_Bank
N=256;
Mel_Scale = 0:Mel_Flow:(K)*Mel_Flow;
f = ceil((exp(Mel_Scale/1125)-1)*700);                               %Return from Mel_Scale
Fil_Bank = zeros(N,20);
step = Fs/N;
for i = 1:20
    s = 1/((f(i+1)/step)-(f(i)/step));
    for j = ceil(f(i)/step):(f(i+1)/step)
        Fil_Bank(j+1,i) = (j-(f(i)/step))*s;
    end
    for k = ceil(f(i+1)/step)+1:2*(f(i+1)/step)-(f(i)/step)
        Fil_Bank(k+1,i) = 1-(k-(f(i+1)/step))*s;
    end
end
Mel_Spec = (Pow_Spec'*Fil_Bank)';                                   %Mel_Spectrum
end