function c=vqlbg(v,k)
%v contains training data vectors (one per column)
% k is number of c  entroids required
%Outputs: c contains the result VQ codebook (k columns, one for each centroids)
c=mean(v,2);
e=0.01;
c(:,1)=c(:,1)*(1+e);
c(:,1)=c(:,1)*(1-e);
% Nearest Neighbour Searching. Given a current codebook 'c', assign each training vector in 'v' with the closest codeword.
%Using the function disteu2, the distances between these vectors (v and c) are computed.
d=disteu(v,c);
[m,index]=min(d,[],2);
[rows,cols]=size(c);
% The mean of the vectors are found using the mean function.
for j=1:cols
    c(:,j)=mean(v(:,find(index==j)),2);
end
% for each training vector, find the closest codeword using the min  function.
n=1;
n=n*2;
while cols<16
    for i=1:cols
        c(:,i)=c(:,i)+c(:,i)*e;
        c(:,i+n)=c(:,i)-c(:,i)*e;
        d=disteu(v,c);
        [m,i]=min(d,[],2);
        [rows,cols]=size(c);
    end
    % The centroids of the vectors are found using the mean function.
    for j=1:cols
        if find(i==j)~isempty(c);
            c(:,j)=mean(v(:,find(i==j)),2);
        end
    end
    n=n*2;
end